import React from "react";
import ContactCard from "./ContactCard";

const ContactList = (props) => {
    const deleteContactHandler = (id) => {
        props.getContactId(id);
    };
    const renderContactList = props.contact.map((contacts) => {
            return (
                <ContactCard contact={contacts} clickHandler={ deleteContactHandler } key={contacts.id}></ContactCard>
            );
        }
    )
    return (
        <div className="ui celled list">{renderContactList}</div>
    )
}

export default ContactList;